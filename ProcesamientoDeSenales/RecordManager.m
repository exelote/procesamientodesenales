//
//  RecordManager.m
//  ProcesamientoDeSenales
//
//  Created by Exequiel on 11/6/15.
//  Copyright (c) 2015 Exequiel. All rights reserved.
//

#import "RecordManager.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>

#import <Firebase/Firebase.h>

#define kDefaultTimeBetweenSamples 1

@interface RecordManager()
@property(nonatomic,strong)AVAudioRecorder *recorder;
@property(nonatomic,strong)NSMutableArray *allDecibels;
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,strong)Firebase *myRootRef;
@property(nonatomic,strong)CLLocationManager *locationManager;

@end

@implementation RecordManager

#pragma mark - Init

+ (instancetype)sharedInstance{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [self new];
    });
    return sharedInstance;
}

- (instancetype)init{
    self = [super init];
    if (self) {
         self.myRootRef = [[Firebase alloc] initWithUrl:@"https://mapa-ruido.firebaseio.com"];
        self.allDecibels = [NSMutableArray new];
        self.timeBetweenSamples = kDefaultTimeBetweenSamples;
        [self loadLocationManager];
        [self loadAudioRecorder];
    }
    return self;
}

- (void)loadLocationManager{
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
}

- (void)loadAudioRecorder{
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    NSDictionary* recorderSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [NSNumber numberWithInt:kAudioFormatAppleIMA4],AVFormatIDKey,
                                      [NSNumber numberWithInt:44100],AVSampleRateKey,
                                      [NSNumber numberWithInt:1],AVNumberOfChannelsKey,
                                      [NSNumber numberWithInt:16],AVLinearPCMBitDepthKey,
                                      [NSNumber numberWithBool:NO],AVLinearPCMIsBigEndianKey,
                                      [NSNumber numberWithBool:NO],AVLinearPCMIsFloatKey,
                                      nil];
    NSError* error = nil;
    self.recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL URLWithString:[NSTemporaryDirectory() stringByAppendingPathComponent:@"tmp.caf"]]  settings:recorderSettings error:&error];
    self.recorder.meteringEnabled = YES;

}
#pragma mark - Logic
- (void)takeSample{
    [self.recorder updateMeters];
    [self.allDecibels addObject: @(pow (10, [self.recorder averagePowerForChannel:0] / 20))];

    if ([self.delegate respondsToSelector:@selector(recordManager:tokeNewSample:)]) {
        [self.delegate recordManager:self tokeNewSample:self.allDecibels.lastObject];
    }
    [self sendData];
}

- (NSString *)weekday{
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    return [NSString stringWithFormat:@"%ld",[comp weekday]];
}

- (void)sendData{
    NSString *weekday = [self weekday];
    [[self.myRootRef childByAutoId]setValue:
  @{@"decibel":self.allDecibels.lastObject,
  @"latitude":@(self.locationManager.location.coordinate.latitude),
  @"longitude":@(self.locationManager.location.coordinate.longitude),
  @"date":@([[NSDate date]timeIntervalSince1970]),
  @"weekday":weekday}];
}

#pragma mark - Actions
- (void)start{
    [self.recorder record];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:self.timeBetweenSamples target:self selector:@selector(takeSample) userInfo:nil repeats:YES];
    [self.timer fire];
}

- (void)stop{
    [self.timer invalidate];
    [self.recorder stop];
}



@end
