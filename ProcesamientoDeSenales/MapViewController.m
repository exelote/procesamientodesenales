//
//  SecondViewController.m
//  ProcesamientoDeSenales
//
//  Created by Exequiel on 11/6/15.
//  Copyright (c) 2015 Exequiel. All rights reserved.
//

#import "MapViewController.h"

@interface MapViewController ()
@property(nonatomic,weak)IBOutlet UIWebView *webView;
@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://mapa-ruido.firebaseapp.com/"]]];
}

@end
