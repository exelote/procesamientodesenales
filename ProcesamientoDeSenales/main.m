//
//  main.m
//  ProcesamientoDeSenales
//
//  Created by Exequiel on 11/6/15.
//  Copyright (c) 2015 Exequiel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
