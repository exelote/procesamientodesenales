//
//  FirstViewController.m
//  ProcesamientoDeSenales
//
//  Created by Exequiel on 11/6/15.
//  Copyright (c) 2015 Exequiel. All rights reserved.
//

#import "RecordViewController.h"
#import "RecordManager.h"
#import "Barra.h"

@interface RecordViewController ()<RecordManagerDelegate>
@property(nonatomic,weak)IBOutlet UILabel *decibelLabel;
@property(nonatomic,weak)IBOutlet Barra *decibelBar;
@end

@implementation RecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[RecordManager sharedInstance] setDelegate:self];
    self.decibelBar.mode = BarraModoHorizontal;
}

- (IBAction)changeRecordState:(UISwitch *)sender{
    if (sender.isOn) {
        [[RecordManager sharedInstance]start];
    }else{
        [[RecordManager sharedInstance]stop];
    }
}

- (void)recordManager:(RecordManager *)recordManager tokeNewSample:(NSNumber *)sample{
    self.decibelLabel.text = [NSString stringWithFormat:@"%.5f",[sample floatValue]];
    [self.decibelBar setValorAnimated:@([sample floatValue])];
}

@end
