//
//  RecordManager.h
//  ProcesamientoDeSenales
//
//  Created by Exequiel on 11/6/15.
//  Copyright (c) 2015 Exequiel. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RecordManagerDelegate;

@interface RecordManager : NSObject
@property(nonatomic,assign)NSTimeInterval timeBetweenSamples;
@property(nonatomic,weak)id<RecordManagerDelegate>delegate;

+ (instancetype)sharedInstance;

- (void)start;

- (void)stop;

@end

@protocol RecordManagerDelegate <NSObject>
@optional
-(void)recordManager:(RecordManager *)recordManager tokeNewSample:(NSNumber *)sample;
@end;

